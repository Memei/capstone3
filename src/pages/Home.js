import { useState, useEffect } from 'react';
import ProductCard from '../components/ProductCard'
import { Carousel, Container } from 'react-bootstrap';
import '../App.css';
import '../Source.css'

export default function Home() {



	return (
		<>
		<Container fluid className="mt-3">
			<Carousel variant="dark">
				  <Carousel.Item>
					<img
					className="d-block w-100"
					  src="https://cdn.pixabay.com/photo/2016/08/04/09/05/coming-soon-1568623_960_720.jpg"
					  alt="First slide"
					/>
				  </Carousel.Item>

			      <Carousel.Item>
			        <img
			        className="d-block w-100"
			          src="https://cdn.pixabay.com/photo/2016/08/04/09/05/coming-soon-1568623_960_720.jpg"
			          alt="Second slide"
			        />
			      </Carousel.Item>

			      <Carousel.Item>
			        <img
			        className="d-block w-100"
			          src="https://cdn.pixabay.com/photo/2016/08/04/09/05/coming-soon-1568623_960_720.jpg"
			          alt="Third slide"
			        />
			      </Carousel.Item>
			</Carousel>

			<h1 className='text-center mt-3'>Featured Products</h1>
		</Container>
		</>
	);	
	
}
